const { ServiceProvider } = require.main.require('@adonisjs/fold')

const i18n = require('../src/i18n')

class GraphQLi18nProvider extends ServiceProvider {
  register () {
    this.app.bind('Adonis/GraphQL/Middleware/i18n', () => i18n)
  }
}

module.exports = GraphQLi18nProvider
