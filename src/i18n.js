const i18n = (next) => (root, args, context, info) => {
  const LocalizedModel = use('LocalizedModel')
  const config = use('Config').get('locales')
  const locale = (i18n && i18n.locale) || context.locale || config.locales.locale
  LocalizedModel.currentLocale = locale

  return next(root, args, { locale, ...context }, info)
}

module.exports = i18n
